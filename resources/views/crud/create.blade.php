<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Form Data Karyawan</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('assets/style.css') }}">
</head>

<body>
    <div class="form-holder">
        <div class="form-content">
            <div class="form-items">


                <h3 class="text-center">Tambah Data Karyawan</h3>

                <form action="/crud/simpan" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="col-md-12">
                        <input class="form-control" type="text" name="nama" placeholder="Nama Karyawan" required>
                    </div>
                    <div class="col-md-12">
                        <input class="form-control" type="text" maxlength="15" name="no" placeholder="No karyawan"
                            required>
                    </div>
                    <div class="col-md-12">
                        <input class="form-control" type="text" maxlength="15" name="telp" placeholder="Nomor Telepon"
                            required>
                    </div>
                    <div class="col-md-12">
                        <input class="form-control" type="text" name="jabatan" placeholder="Jabatan Karyawan" required>

                    </div>
                    <div class="col-md-12 mt-3">
                        <input class="form-control" type="text" name="divisi" placeholder="Divisi Karyawan" required>
                    </div>
                    <div class="row ">
                        <div class="col-md-3">
                            <div class="form-button mt-3">
                                <button id="submit" type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-button mt-3">
                                <button id="reset" type="reset" class="btn btn-primary">Reset</button>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-button mt-3">
                                <a href="/home" class="btn btn-primary">Back</a>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous">
    </script>
</body>

</html>