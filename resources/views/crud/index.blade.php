<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Simple CRUD</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<div class="form-holder">
    <div class="form-content">
        <div class="form-items">
            <table class="table  text-white">
                <h3 class="text-center">Data Karyawan</h3>
                <a href="/crud/create" class="btn btn-primary ">Tambah Data</a>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nama Karyawan</th>
                        <th>No Karyawan</th>
                        <th>No Telepon</th>
                        <th>Jabatan</th>
                        <th>Divisi</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                 <!-- perulangan untuk seluruh item yang ada di database syntax ala blade-->
            @foreach ($karyawan as $row )
            <tr>
                <td>{{$row->id}}</td>
                <td>{{$row->nama_karyawan}}</td>
                <td>{{$row->no_karyawan}}</td>
                <td>{{$row->no_telp_karyawan}}</td>
                <td>{{ $row->jabatan_karyawan }}</td>
                <td>{{ $row->divisi_karyawan }}</td>
                <td>
                    <a href="/crud/edit/{{$row->id}}" class="btn btn-primary">Edit</a>
                    <a href="/crud/hapus/{{ $row->id }}" class="btn btn-primary">Delete</a>
                </td>
            </tr>
            @endforeach
           
                </tbody>
            </table>
           

        </div>
    </div>
</div>
<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous">
</script>
</body>
</html>
