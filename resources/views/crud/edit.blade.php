<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Simple CRUD</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('assets/style.css') }}">
</head>
<div class="form-holder">
    <div class="form-content">
        <div class="form-items">
            <h3 class="text-center">Edit Data Karyawan</h3>
            @foreach ($karyawan as $row )
            <form action="/crud/simpan" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" value="{{ $row->id }}">
                <div class="col-md-12">
                    <input class="form-control" type="text" name="nama" value="{{ $row->nama_karyawan }}" required>
                </div>
                <div class="col-md-12">
                    <input class="form-control" type="text" maxlength="15" name="no" value="{{ $row->no_karyawan }}"
                        required>
                </div>
                <div class="col-md-12">
                    <input class="form-control" type="text" maxlength="15" name="telp"
                        value="{{ $row->no_telp_karyawan }}" required>
                </div>
                <div class="col-md-12">
                    <input class="form-control" type="text" name="jabatan" value="{{ $row->jabatan_karyawan }}"
                        required>

                </div>
                <div class="col-md-12 mt-3">
                    <input class="form-control" type="text" name="divisi" value="{{ $row->divisi_karyawan }}" required>
                </div>
                <div class="row ">
                    <div class="col-md-3">
                        <div class="form-button mt-3">
                            <button id="submit" type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-button mt-3">
                            <button id="reset" type="reset" class="btn btn-primary">Reset</button>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-button mt-3">
                            <a href="/home" class="btn btn-primary">Back</a>
                        </div>
                    </div>
                </div>

            </form>
            @endforeach
        </div>
    </div>
</div>
<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous">
</script>
<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous">
</script>
</body>

</html>