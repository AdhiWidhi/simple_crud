<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CrudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //mengambil data
        $karyawan = DB::table('karyawan')->get();
        //menampilkan data di view index
        return view('crud.index', ['karyawan' => $karyawan]);
    }
    // function create fungsinya untuk menampilkan view dari Create
    public function create()
    {
        return view('crud.create');
    }

    // function untuk menyimpan data inputan ke database
    public function simpan(Request $request)
    {
        DB::table('karyawan')->insert([
            //insert data ke tabel karyawan
            'nama_karyawan' => $request->nama,
            'no_karyawan' => $request->no,
            'no_telp_karyawan' => $request->telp,
            'jabatan_karyawan' => $request->jabatan,
            'divisi_karyawan' => $request->divisi
        ]);
        return redirect('/home');
    }

    // function edit untuk parameter yang dipakek id karena di route menggunakan id sebagai acuan link url data karyawan yang diedit
    public function edit($id)
    {
        $karyawan = DB::table('karyawan')->where('id', $id)->get();
        return view('crud.edit', ['karyawan' => $karyawan]);
    }


    //karena diatas untuk menampilkan editnya sudah selesai maka update di databasenya kalau data dengann $id yang diedit terupdate didatabase
    public function update(Request $request)
    {
        DB::table('karyawan')->where('id', $request->id)->update([
            'nama_karyawan' => $request->nama,
            'no_karyawan' => $request->no,
            'no_telp_karyawan' => $request->telp,
            'jabatan_karyawan' => $request->jabatan,
            'divisi_karyawan' => $request->divisi
        ]);
        return redirect('/home');
    }

    public function destroy($id)
    {
        DB::table('karyawan')->where('id', $id)->delete();
        return redirect('/home');
    }
}